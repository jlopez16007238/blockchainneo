app.controller("ctrlConsultar", function ($scope, $http, $window, ServicioLogin, $location, $mdToast) {
    $scope.firstname = $window.localStorage['local.firstname'];
      $scope.secondname = $window.localStorage['local.secondname'];
      $scope.lastname = $window.localStorage['local.lastname'];
      $scope.secondlname =  $window.localStorage['local.secondlname'];
      $scope.username = $window.localStorage['local.username'];
      $scope.rol = $window.localStorage['local.rol'];
    $scope.headTitle = 'Consultar';
    $scope.search = function () {
        
        if ($scope.txtSearch) {
            $scope.loading = true;
            $http.get("http://localhost:8081/contracts/" + $scope.txtSearch)
            .then(function (dataBody) {
                let data = dataBody.data;
                $scope.txtCodigo = data.codigoContrato;
                $scope.txtPropietario = data.nombrePropietario;
                $scope.DPI = data.dpiPropietario;
                $scope.txtInquilino = data.nombreInquilino;
                $scope.txtDPInquilino = data.dpiInquilino;
                $scope.txtMonto = data.monto;
                $scope.txtFechaPago = new Date(data.fechaDePago);
                $scope.txtNoReferencia = data.noRefPago;
                $scope.txtCheckPago = data.confirmacionPago;
                $scope.loading = false;
            }, function error() {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent("Ocurrió un error")
                        .position('top right')
                        .highlightClass('md-danger')
                        .hideDelay(5000)
                );
                $scope.loading = false;
            });
        } else {
            $mdToast.show(
                $mdToast.simple()
                    .textContent("Ingrese un código porfavor")
                    .position('top right')
                    .highlightClass('md-accent')
                    .hideDelay(5000)
            );
        }
    }

    $scope.modificar = function () {
        let data = {
            codigoContrato: $scope.txtCodigo,
            nombrePropietario: $scope.txtPropietario,
            dpiPropietario: $scope.DPI,
            nombreInquilino: $scope.txtInquilino,
            dpiInquilino: $scope.txtDPInquilino,
            monto: $scope.txtMonto,
            fechaDePago: $scope.txtFechaPago,
            noRefPago: $scope.txtNoReferencia,
            confirmacionPago: $scope.txtCheckPago 
        }
        console.log(data);
        if ($scope.txtCodigo) {
            $scope.loading = true;
        $http.post("http://localhost:8081/contracts/add", data)
            .then(function (dataBody) {
                console.log(dataBody)
                if (dataBody.data.codigoContrato !== undefined) {
                    let data = dataBody.data;
                    $scope.txtCodigo = data.codigoContrato;

                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Modificado satisfactoriamente")
                            .position('top right')
                            .highlightClass('md-accent')
                            .hideDelay(5000)
                    );
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(dataBody.data)
                            .position('top right')
                            .highlightClass('md-accent')
                            .hideDelay(5000)
                    );
                }
                $scope.loading = false;

            }, function error() {
                $mdToast.show(
                    $mdToast.simple()
                        .textContent("Ocurrió un error")
                        .position('top right')
                        .highlightClass('md-accent')
                        .hideDelay(5000)
                );
                $scope.loading = false;
            });
        } else {
            $mdToast.show(
                $mdToast.simple()
                    .textContent("Realice un búsqueda")
                    .position('top right')
                    .highlightClass('md-accent')
                    .hideDelay(5000)
            );
        }
    }

    $scope.closeSession = function() {
        $window.localStorage['local.username'] = null;
        $window.localStorage['local.firstname'] = null;
        $window.localStorage['local.secondname'] = null;
        $window.localStorage['local.lastname'] = null;
        $window.localStorage['local.secondlname'] = null;
        $window.localStorage['local.username'] = null;
        $window.localStorage['local.rol'] = null;

        $location.url('/login');
    }

    $scope.profile = function() {
        $location.url('/profile');
    }
});