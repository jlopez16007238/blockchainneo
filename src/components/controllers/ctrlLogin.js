app.controller("ctrlLogin", function ($scope, $http, $window, $location, ServicioLogin, $mdToast){
	$scope.login = function() {
		let usuarios = [
			{
				primerNombre: 'Juan',
				segundoNombre: 'Emanuel',
				primerApellido: 'López',
				segundoApellido: 'Pacheco',
				username: 'JLopez',
				pass: '123',
				rol: 1
			},
			{
				primerNombre: 'Diego',
				segundoNombre: 'Javier',
				primerApellido: 'Barrios',
				segundoApellido: 'Barrera',
				username: 'DBarrios',
				pass: '123',
				rol: 1
			},
			{
				primerNombre: 'Juan',
				segundoNombre: 'Pablo',
				primerApellido: 'Solares',
				segundoApellido: 'Maldonado',
				username: 'JSolares',
				pass: '123',
				rol: 1
			},
			{
				primerNombre: 'Mario',
				segundoNombre: 'Alexander',
				primerApellido: 'Tax',
				segundoApellido: 'Chojolan',
				username: 'MTax',
				pass: '123',
				rol: 1
			},
			{
				primerNombre: 'Alejandro',
				segundoNombre: 'Alexander',
				primerApellido: 'Arango',
				segundoApellido: 'Aresti',
				username: 'AArango',
				pass: '123',
				rol: 1
			},
			{
				primerNombre: 'Byron',
				segundoNombre: 'Alberto',
				primerApellido: 'Carrera',
				segundoApellido: 'Morales',
				username: 'BCarrera',
				pass: '123',
				rol: 2
			},
			{
				primerNombre: 'Juan',
				segundoNombre: 'Carlos',
				primerApellido: 'Gutierrez',
				segundoApellido: 'Campos',
				username: 'JGutierrez',
				pass: '123',
				rol: 2
			}
		];

		
		datosCredenciales = {usuario: $scope.usuario , clave : $scope.pass};
		let usr = null;
		
		for(let i=0; i < usuarios.length; i++){
			if(usuarios[i].username == datosCredenciales.usuario && usuarios[i].pass == datosCredenciales.clave) {
				usr = usuarios[i];
			}
		}
		
		if(usr) {
			if(usr.rol == 1) {
				$location.url('/inicio');
				$window.localStorage['local.firstname'] = usr.primerNombre;
				$window.localStorage['local.secondname'] = usr.segundoNombre;
				$window.localStorage['local.lastname'] = usr.primerApellido;
				$window.localStorage['local.secondlname'] = usr.segundoApellido;
				$window.localStorage['local.username'] = usr.username;
				$window.localStorage['local.rol'] = usr.rol;
			} else {
				$location.url('/inicio');
				$window.localStorage['local.firstname'] = usr.primerNombre;
				$window.localStorage['local.secondname'] = usr.segundoNombre;
				$window.localStorage['local.lastname'] = usr.primerApellido;
				$window.localStorage['local.secondlname'] = usr.segundoApellido;
				$window.localStorage['local.username'] = usr.username;
				$window.localStorage['local.rol'] = usr.rol;
			}
		} else {
			$mdToast.show(
                $mdToast.simple()
                    .textContent("No coinciden su usuario o contraseña, intentelo nuevamente")
                    .position('top right')
                    .highlightClass('md-accent')
                    .hideDelay(5000)
            );
		}
	}
});