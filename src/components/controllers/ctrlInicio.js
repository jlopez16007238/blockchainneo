app.controller("ctrlInicio", function ($scope, $http, $window, ServicioLogin, $location) {
  $scope.firstname = $window.localStorage['local.firstname'];
      $scope.secondname = $window.localStorage['local.secondname'];
      $scope.lastname = $window.localStorage['local.lastname'];
      $scope.secondlname =  $window.localStorage['local.secondlname'];
      $scope.username = $window.localStorage['local.username'];
      $scope.rol = $window.localStorage['local.rol'];
  $scope.headTitle = 'Inicio';

  $scope.closeSession = function() {
    $window.localStorage['local.username'] = null;
    $window.localStorage['local.firstname'] = null;
    $window.localStorage['local.secondname'] = null;
    $window.localStorage['local.lastname'] = null;
    $window.localStorage['local.secondlname'] = null;
    $window.localStorage['local.username'] = null;
    $window.localStorage['local.rol'] = null;
    
    $location.url('/login');
  }

  $scope.profile = function() {
    $location.url('/profile');
  }

});