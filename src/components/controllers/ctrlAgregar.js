app.controller("ctrlAgregar", function ($scope, $http, $window, ServicioLogin, $location, $mdToast){
    $scope.firstname = $window.localStorage['local.firstname'];
      $scope.secondname = $window.localStorage['local.secondname'];
      $scope.lastname = $window.localStorage['local.lastname'];
      $scope.secondlname =  $window.localStorage['local.secondlname'];
      $scope.username = $window.localStorage['local.username'];
      $scope.rol = $window.localStorage['local.rol'];
    $scope.headTitle = 'Agregar';
    $scope.agregar = function() {
        let data = {
            codigoContrato: $scope.txtCodigo,
            nombrePropietario: $scope.txtPropietario,
            dpiPropietario: $scope.DPI,
            nombreInquilino: $scope.txtInquilino,
            dpiInquilino: $scope.txtDPInquilino,
            monto: $scope.txtMonto,
            fechaDePago: $scope.txtFechaPago,
            noRefPago: $scope.txtNoReferencia,
            confirmacionPago: $scope.txtCheckPago 
        }
        $scope.loading = true;
        $http.post("http://localhost:8081/contracts/add", data)
        .then(function(result){
            console.log(result)
            if (result.data !== undefined) {
				$mdToast.show(
                    $mdToast.simple()
                        .textContent("Agregado satisfactoriamente")
                        .position('top right')
                        .highlightClass('md-accent')
                        .hideDelay(5000)
                );
			} else {
				$mdToast.show(
                    $mdToast.simple()
                        .textContent("Hubo un error cuando se quizo agregar")
                        .position('top right')
                        .highlightClass('md-accent')
                        .hideDelay(5000)
                );
            }
            $scope.loading = false;
        }, function error(){
            $mdToast.show(
                $mdToast.simple()
                    .textContent("Ocurrió un error")
                    .position('top right')
                    .highlightClass('md-accent')
                    .hideDelay(5000)
            );
            $scope.loading = false;
        });
    }
    $scope.closeSession = function() {
        $window.localStorage['local.username'] = null;
        $window.localStorage['local.firstname'] = null;
        $window.localStorage['local.secondname'] = null;
        $window.localStorage['local.lastname'] = null;
        $window.localStorage['local.secondlname'] = null;
        $window.localStorage['local.username'] = null;
        $window.localStorage['local.rol'] = null;

        $location.url('/login');
    }
    $scope.profile = function() {
        $location.url('/profile');
    }
  });