var app = angular.module("neoblockchain", ['ngRoute', 'ngMaterial', 'ngMessages']);

app.config(function($routeProvider) {
	$routeProvider
	.when("/login", {templateUrl:"views/login.html", controller:"ctrlLogin"})
	.when("/inicio", {templateUrl: "views/inicio.html", controller: "ctrlInicio"})
	.when("/agregar", {templateUrl: "views/contracts/add.html", controller: "ctrlAgregar"})
	.when("/consultar", {templateUrl: "views/contracts/contracts.html", controller: "ctrlConsultar"})
	.when("/profile", {templateUrl: "views/profile/profile.html", controller: "ctrlProfile"})
	.otherwise({redirectTo:'/login'});
});

app.factory("ServicioLogin", function() {
	return {
		data: {}
	};
});