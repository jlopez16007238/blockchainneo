var gulp = require('gulp'),
    connect = require('gulp-connect'),
    historyApiFallback = require('connect-history-api-fallback'),
    livereload = require('gulp-livereload'),
    inject = require('gulp-inject'),
    wiredep = require('wiredep').stream;

gulp.task('server', function() {
    connect.server({
        root: './src',
        port: '3008',
        livereload: 'true',
        middleware: function(connect, opt) {
            return [historyApiFallback({})];
        }
    });
});

gulp.task('html', function() {
    gulp.src('./src/*.html')
    .pipe(connect.reload())
    .pipe(livereload());
});

gulp.task('inject', function(){
    var sources = gulp.src(['./src/components/**/*.js', './src/styles/**/*.css']);
    return gulp.src('index.html', {
        cwd: './src'
    })
    .pipe(inject(sources, {ignorePath: '/src'}))
    .pipe(gulp.dest('./src'));
})

gulp.task('wiredep', function() {
    gulp.src('./src/index.html')
    .pipe(wiredep({
        directory: './src/vendor',
        onError: function(err) {
            console.log(err);
        }
    }))
    .pipe(gulp.dest('./src'));
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(['./src/*.html'], ['html']);
    gulp.watch(['./src/views/**/*.html'], ['html']);
    gulp.watch(['./src/components/**/*.js'], ['inject']);
    gulp.watch(['./src/styles/**/*.css'], ['inject']);
    gulp.watch(['./bower.json'], ['wiredep']);
});

gulp.task('default', ['server', 'inject', 'wiredep', 'watch'], function(){

});
